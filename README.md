 
# Java Apache Bean en Docker
 
Este repositorio muestra los pasos que se deben ejecutar para crear un contenedor en Docker que corra Apache Bean en Java. También muestra cómo usar Maven para crear y configurar correctamente el proyecto de java.
 
 
### Contenedor de Docker
 
Se debe crear un contenedor que tenga instalado el paquete Maven, para poder correr el proyecto de java y también importar las dependencias de Apache Maven.
 
Recordemos que Maven es un gestor de paquetes para java, con el te puedes olvidar de tener que importar .JAR a tus proyectos para instalar dependencias.
 
Hay una [imagen de docker](https://hub.docker.com/_/maven) que tiene configurado Maven, no tenemos que configurar nada, solo extender la imagen. Para seleccionar la versión de la imagen que quieres descargar, vas a la página de docker hub y revisas los tags; para este caso necesitamos Java 8, entonces seleccionamos la imagen más reciente con Java  8.
 
!["Selección de la versión de la imagen de docker"](assets/tag docker.png)
 
Lo siguiente es correr `/bin/bash` un contenedor de docker con la imagen de Maven que seleccionamos.
Con la opción -v se crea un volumen para tener una carpeta compartida entre el el host y el contenedor.
 
```bash
docker-app]$ docker run -it     -v /docker-app:/app  -w /app     maven:3.6.3-jdk-8 /bin/bash
```
 
Recuerda que para crear volúmenes [solo se aceptan rutas absolutas](https://github.com/moby/moby/issues/4830).
 
Como resultado deberíamos estar dentro del contenedor.
 
```bash
root@9d900f72d8e5:/app#
```


 
### Creación del proyecto de java en Maven
 
Estando en el contenedor ya podemos hacer uso del comando `mvn`, lo siguiente será crear el proyecto de java con Maven. En la [documentacion oficial de Maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) esta la informacion de como se crean diferentes tipos de proyectos con Maven, en este caso se necesita crear un proyecto basico en Java, por ende usaremos el siguiente comando:
 
```
mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```
El resultado de este comando será un proyecto con  la siguiente estructura:
 
```
my-app
|-- pom.xml
`-- src
   |-- main
   |   `-- java
   |       `-- com
   |           `-- mycompany
   |               `-- app
   |                   `-- App.java
   `-- test
       `-- java
           `-- com
               `-- mycompany
                   `-- app
                       `-- AppTest.java
```
Con un pom.xml básico                       
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
 <modelVersion>4.0.0</modelVersion>
 <groupId>com.mycompany.app</groupId>
 <artifactId>my-app</artifactId>
 <version>1.0-SNAPSHOT</version>
 <properties>
   <maven.compiler.source>1.7</maven.compiler.source>
   <maven.compiler.target>1.7</maven.compiler.target>
 </properties>
 <dependencies>
   <dependency>
     <groupId>junit</groupId>
     <artifactId>junit</artifactId>
     <version>4.12</version>
     <scope>test</scope>
   </dependency>
 </dependencies>
</project>
```
### Importar paquetes de Apache Beam
El siguiente paso es agregar los paquetes de `Apache Beam` al proyecto incluyendolos en el `pom.xml`. En la [pagina de repositorios de maven](https://mvnrepository.com) se buscan los siguientes paquetes y se escoge la versión más reciente:
 - Beam SDKs Java Core
 - Beam Runners Direct Java
 
Para agregar un paquete al proyecto, después de buscarlo se selecciona la versión, se  copia el código de maven y se  agrega en el apartado `dependencies` en el archivo  `pom.xml`.

!["Importar paquete de Maven"](assets/Seleccion del la version del paquete maven.png)

!["Importar paquete de Maven 1"](assets/Copiar codigo mvn.png)

 
El código de maven para importar Beam SDKs Java Core en su version 2.25.0 es:
```xml
<!-- https://mvnrepository.com/artifact/org.apache.beam/beam-sdks-java-core -->
<dependency>
   <groupId>org.apache.beam</groupId>
   <artifactId>beam-sdks-java-core</artifactId>
   <version>2.25.0</version>
</dependency>
```
 
El código de maven para importar Beam Runners Direct Java  en su versión 2.25.0 es:
```xml
<!-- https://mvnrepository.com/artifact/org.apache.beam/beam-sdks-java-core -->
<!-- https://mvnrepository.com/artifact/org.apache.beam/beam-runners-direct-java -->
<dependency>
   <groupId>org.apache.beam</groupId>
   <artifactId>beam-runners-direct-java</artifactId>
   <version>2.25.0</version>
   <scope>test</scope>
</dependency>
 
```
 
El `pom.xml` con las dependencias agregadas queda de la siguiente forma:
 
```xml
<?xml version="1.0" encoding="UTF-8"?>
 
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
 <modelVersion>4.0.0</modelVersion>
 
 <groupId>com.mycompany.app</groupId>
 <artifactId>my-app</artifactId>
 <version>1.0-SNAPSHOT</version>
 
 <name>my-app</name>
 <!-- FIXME change it to the project's website -->
 <url>http://www.example.com</url>
 
 <properties>
   <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
   <maven.compiler.source>8</maven.compiler.source>
   <maven.compiler.target>8</maven.compiler.target>
 </properties>
 
 
 <dependencies>
    <dependency>
     <groupId>junit</groupId>
     <artifactId>junit</artifactId>
     <version>4.11</version>
     <scope>test</scope>
   </dependency>
    <!-- Beam -->
   <!-- https://mvnrepository.com/artifact/org.apache.beam/beam-sdks-java-core -->
   <dependency>
       <groupId>org.apache.beam</groupId>
       <artifactId>beam-sdks-java-core</artifactId>
       <version>2.25.0</version>
   </dependency>
 
       <!-- https://mvnrepository.com/artifact/org.apache.beam/beam-runners-direct-java -->
   <dependency>
       <groupId>org.apache.beam</groupId>
       <artifactId>beam-runners-direct-java</artifactId>
       <version>2.25.0</version>
       <scope>test</scope>
   </dependency>
 
   <!-- https://mvnrepository.com/artifact/org.apache.beam/beam-sdks-java-io-google-cloud-platform -->
   <dependency>
       <groupId>org.apache.beam</groupId>
       <artifactId>beam-sdks-java-io-google-cloud-platform</artifactId>
       <version>2.25.0</version>
   </dependency>
</dependencies>
 
 <build>
       ....Código del build que viene por defecto.
 </build>
</project>
 
```
De esta misma forma puedes importar cualquier paquete que necesites en el proyecto, recuerda, debes buscar la página de repositorios de maven [https://mvnrepository.com/](https://mvnrepository.com/).
 
Nota: Recuerda que al hacer un volumen, los cambios que hagas en la carpeta /app del contenedor se verán reflejados en la carpeta de tu computadora que enlazaste.
 
 
### Compilar y ejecutar el proyecto de Maven.
Antes de compilar buscamos el Main del proyecto,en este caso está en src/main/java/com/mycompany/app y se llama App, e importamos algunas dependencias de apache beam.
```java
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.FlatMapElements;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptors;
 
```
Lo siguiente será compilar el proyecto(En el bash del contenedor).
 
para compilar el proyecto se ejecuta el comando:
 
```bash
root@5ce49c5288b9:/app# cd my-app/
root@5ce49c5288b9:/app/my-app# mvn package
[INFO] Scanning for projects...
[INFO]
[INFO] ----------------------< com.mycompany.app:my-app >----------------------
[INFO] Building my-app 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
```
 
El resultado de la ejecución del comando anterior es una carpeta llamada `target`, en donde  entre otras cosas se crea el .jar del proyecto. En este caso: `my-app-1.0-SNAPSHOT.jar`
 
Para ejecutar el main del proyecto, primero abre el archivo main en un editor de texto y copia el paquete donde está:
Copiar codigo mvn.png
 
!["Buscar el Main"](assets/Ubicacion del main.png)
 
Para ejecutar el proyecto debes ejecutar el siguiente comando: ``java -cp `Dirección del .jar` `paquete del proyecto`.App``. En este caso quedaría de la siguiente forma:
 
```bash
root@5ce49c5288b9:/app/my-app# java -cp target/my-app-1.0-SNAPSHOT.jar  com.mycompany.app.App
Funcionando
root@5ce49c5288b9:/app/my-app#
```
Esto quiere decir que ya podemos usar Apache beam en el proyecto y lo podemos correr dentro de un contenedor, ya que no marco error al importar las dependencias al Main.
 
### Organización del Dockerfile y el docker-compose.yml
 
El `Dockerfile` queda de la siguiente forma:
 
```docker
FROM maven:3.6.3-jdk-8
COPY ./my-app /app
WORKDIR /app
 
RUN mvn package
 
CMD ["java","-cp", "target/my-app-1.0-SNAPSHOT.jar" ,"com.mycompany.app.App"]
 
```

EL `docker-compose.yml` queda de la siguiente forma:
 
```yml
version: '3.3'
 
services:
 db_conne:
   build:
     context: .
     dockerfile: Dockerfile
   volumes:
     - ./my-app:/app
```
Por último para ejecutar el proyecto usando el docker-compose.yml usamos el siguiente comando:
 
```bash
[user@host docker-app]$ docker-compose up
Building db_conne
Step 1/5 : FROM maven:3.6.3-jdk-8
---> 5bbbc7693372
Step 2/5 : COPY ./my-app /app
---> Using cache
---> 3eeb83c4042e
Step 3/5 : WORKDIR /app
---> Using cache
---> c1298a41b008
Step 4/5 : RUN mvn package
---> Using cache
---> aaf9b50425b2
Step 5/5 : CMD ["java","-cp", "target/my-app-1.0-SNAPSHOT.jar" ,"com.mycompany.app.App"]
---> Running in be8215e7103c
Removing intermediate container be8215e7103c
---> f255ed3e4590
 
Successfully built f255ed3e4590
Successfully tagged docker-app_db_conne:latest
 
Starting docker-app_db_conne_1 ... done
Attaching to docker-app_db_conne_1
db_conne_1  | Funcionando
docker-app_db_conne_1 exited with code 0
```


 
### Recomendaciones
 
1. Puedes usar correr /bin/bash/ en el contenedor de docker para desarrollar, al estar dentro del contenedor tienes el paquete Maven(mvn), y lo podrás usar como si estuviera en tu máquina.
2. Si quieres usar un entorno como Eclipse o Netbeans, busca como se usa para desarrollar con maven  e instalalo en tu máquina host.
3. Cuando quieras usar un nuevo paquete en el proyecto buscalo en [https://mvnrepository.com/])(https://mvnrepository.com/).

### Problemas con los permisos en la carpeta my-app

Todo lo que crees desde la carpeta `/app` en el contenedor se reflejara en el host por el volumen pero sera del usuario root, docker hace esto por seguridad, para asegurar que ningun otro proceso vaya a editar la data de tu contenedor.

En este caso se quiere editar esa data porque a data es el codigo fuente de la app, en este caso tienes varias opciones.

1.  Editar el codigo como usuario root(NO RECOMENDABLE).
2.  Copiar y pegar la carpeta my_app para que sea creada por tu usuario normal, y luego cambiar el volumen en el docker-compose
3. Cambiar el duenio del la carpeta `my-app` con el comando chown
    ```bash
        sudo chown usuario -R my-app
    ```
4. Instalar maven en tu maquina host, configurarlo y hacer los pasos de "Creacion del proyecto en Maven" en tu maquina local.

