FROM maven:3.6.3-jdk-8
COPY ./my-app /app
WORKDIR /app

RUN mvn package

# recuerda que este comando cambia dependiendo de el nombre del proyecto, en el readme explico como hacerlo
CMD ["java","-cp", "target/my-app-1.0-SNAPSHOT.jar" ,"com.mycompany.app.SqlserverToBigqueryPipeline"]


